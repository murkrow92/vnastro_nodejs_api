const express = require('express');
const app = express();
const AstroCalculator = require('./AstroCalculator');
const bodyParser = require('body-parser');

const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const GEO_LAT = 21.027764;
const GEO_LONG = 105.83416;
const TIME_ZONE = 7;

app.route('/astro').get((req, res) => {
    const defaultDate = new Date();

    const day = parseInt(req.query.day) || defaultDate.getDate();
    const month = parseInt(req.query.month) || defaultDate.getMonth() + 1;
    const year = parseInt(req.query.year) || defaultDate.getFullYear();
    const longitude = parseFloat(req.query.longitude) || GEO_LONG;
    const latitude = parseFloat(req.query.latitude) || GEO_LAT;

    let hour = defaultDate.getHours();
    if (req.query.hour === '0') {
        hour = 0;
    } else if(req.query.hour) {
        hour = parseInt(req.query.hour);
    }

    let minute = defaultDate.getMinutes();
    if (req.query.minute === '0') {
        minute = 0;
    } else if (req.query.minute){
        minute =  parseInt(req.query.minute);
    }

    let timezone = TIME_ZONE;
    if (req.query.timezone === '0') {
        timezone = 0;
    } if(req.query.timezone){
        timezone = parseFloat(req.query.timezone)
    }

    console.log('query: ', req.query, 'req: ', hour, minute, timezone);

    console.log(
        'query: ',
        'day:',
        day,
        'month:',
        month,
        'year:',
        year,
        'hour:',
        hour,
        'minute:',
        minute,
        timezone,
        longitude,
        latitude
    );
    const result = AstroCalculator.calculate(
        day,
        month,
        year,
        hour,
        minute,
        timezone,
        longitude,
        latitude
    );
    res.send(result);
});

app.listen(port, () => console.log('Example app listening on port 3000!'));
