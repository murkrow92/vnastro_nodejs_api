const AstroHelper = require('./AstroHelper');
const PurpleAirCalculator = require('./PurpleAirCalculator');

const swisseph = require('swisseph');
swisseph.swe_set_ephe_path(__dirname + '/ephe');
// swisseph.swe_set_ephe_path(__dirname + '\\ephe\\');

const HOUSE_METHOD_PLACIDUS = 'P'; //Placidus Method

const flag = swisseph.SEFLG_SPEED | swisseph.SEFLG_MOSEPH;

const NESSUS_OFFSET = swisseph.SE_AST_OFFSET + 7066;
const PHOLUS_OFFSET = swisseph.SE_AST_OFFSET + 5145;
const ORCUS_OFFSET = swisseph.SE_AST_OFFSET + 90482;
const ERIS_OFFSET = swisseph.SE_AST_OFFSET + 136199;
const HAUMEA_OFFSET = swisseph.SE_AST_OFFSET + 136108;
const MAKEMAKE_OFFSET = swisseph.SE_AST_OFFSET + 136472;
const CHARIKLO_OFFSET = swisseph.SE_AST_OFFSET + 10199;
const VARUNA_OFFSET = swisseph.SE_AST_OFFSET + 20000;
const CYLLARUS_OFFSET = swisseph.SE_AST_OFFSET + 52975;
const QUAOAR_OFFSET = swisseph.SE_AST_OFFSET + 50000;
const IXION_OFFSET = swisseph.SE_AST_OFFSET + 28978;
const HYLONOME_OFFSET = swisseph.SE_AST_OFFSET + 10370;
const ASBOLUS_OFFSET = swisseph.SE_AST_OFFSET + 8405;
const PSYCHE_OFFSET = swisseph.SE_AST_OFFSET + 16;
const PERSEPHONE_OFFSET = swisseph.SE_AST_OFFSET + 399;
const PROSEPRPINA_OFFSET = swisseph.SE_AST_OFFSET + 26;

const arr_planets = {
    Sun: swisseph.SE_SUN,
    Moon: swisseph.SE_MOON,
    Mercury: swisseph.SE_MERCURY,
    Venus: swisseph.SE_VENUS,
    Mars: swisseph.SE_MARS,
    Jupiter: swisseph.SE_JUPITER,
    Saturn: swisseph.SE_SATURN,
    Uranus: swisseph.SE_URANUS,
    Neptune: swisseph.SE_NEPTUNE,
    Pluto: swisseph.SE_PLUTO,
    NorthNode: swisseph.SE_TRUE_NODE,
    Chiron: swisseph.SE_CHIRON,
    Ceres: swisseph.SE_CERES,
    Juno: swisseph.SE_JUNO,
    Vesta: swisseph.SE_VESTA,
    Pallas: swisseph.SE_PALLAS,
    Lilith: swisseph.SE_MEAN_APOG,
    Hades: swisseph.SE_HADES,
    Vulcanus: swisseph.SE_VULKANUS,
    Cupido: swisseph.SE_CUPIDO,
    Kronos: swisseph.SE_KRONOS,
    Zeus: swisseph.SE_ZEUS,
    Poseidon: swisseph.SE_POSEIDON,
    Apollon: swisseph.SE_APOLLON,
    Admetos: swisseph.SE_ADMETOS,
    Nessus: NESSUS_OFFSET,
    Pholus: PHOLUS_OFFSET,
    Orcus: ORCUS_OFFSET,
    Eris: ERIS_OFFSET,
    Haumea: HAUMEA_OFFSET,
    Makemake: MAKEMAKE_OFFSET,
    Chariklo: CHARIKLO_OFFSET,
    Varuna: VARUNA_OFFSET,
    Cyllarus: CYLLARUS_OFFSET,
    Quaoar: QUAOAR_OFFSET,
    Ixion: IXION_OFFSET,
    Hylonome: HYLONOME_OFFSET,
    Asbolus: ASBOLUS_OFFSET,
    Psyche: PSYCHE_OFFSET,
    Persephone: PERSEPHONE_OFFSET,
    Proserpina: PROSEPRPINA_OFFSET
};

const NO_ORB = [
    'AC',
    'MC',
    'NorthNode',
    'SouthNode',
    'Vertex',
    'PurpleAir',
    'PartOfFortune',
    'Ceres',
    'Juno',
    'Vesta',
    'Pallas',
    'Lilith',
    'Hades',
    'Vulcanus',
    'Cupido',
    'Kronos',
    'Zeus',
    'Poseidon',
    'Apollon',
    'Admetos',
    'Nessus',
    'Pholus',
    'Orcus',
    'Eris',
    'Haumea',
    'Makemake',
    'Chariklo',
    'Varuna',
    'Cyllarus',
    'Quaoar',
    'Ixion',
    'Hylonome',
    'Asbolus',
    'Psyche',
    'Persephone',
    'Proserpina'
];

const ASPECT_TYPE = [
    { name: 'conjunct', angle: 0, orb: 10 },
    { name: 'square', angle: 90, orb: 10 },
    { name: 'trine', angle: 120, orb: 10 },
    { name: 'opposite', angle: 180, orb: 10 },
    { name: 'sextile', angle: 60, orb: 6 },
    { name: 'semisextile', angle: 30, orb: 3 },
    { name: 'semisquare', angle: 45, orb: 3 },
    { name: 'sesquisquare', angle: 135, orb: 3 },
    { name: 'quincunx', angle: 150, orb: 3 },
    { name: 'quintile', angle: 72, orb: 2 },
    { name: 'biquintile', angle: 144, orb: 2 }
];

class AstroCalculator {
    static calculate(
        day,
        month,
        year,
        hour,
        minute,
        timezone,
        longitude,
        latitude
    ) {
        let result = { Planets: {}, Houses: [] };
        const julday_ut = AstroCalculator.getJulidanDayUT(
            day,
            month,
            year,
            hour,
            minute,
            timezone
        );
        for (const key in arr_planets) {
            swisseph.swe_calc_ut(julday_ut, arr_planets[key], flag, body => {
                // console.log(key, body);
                result.Planets[key] = AstroCalculator.getPlanetResult(
                    body,
                    key
                );
                if (key === 'NorthNode') {
                    const northNodeLongitude = body.longitude;
                    const southNodeLongitude =
                        northNodeLongitude >= 180
                            ? northNodeLongitude - 180
                            : northNodeLongitude + 180;
                    body.longitude = southNodeLongitude;
                    result.Planets.SouthNode = AstroCalculator.getPlanetResult(
                        body,
                        'SouthNode'
                    );
                }
            });
        }

        const houses = swisseph.swe_houses(
            julday_ut,
            latitude,
            longitude,
            HOUSE_METHOD_PLACIDUS
        );
        for (const key in houses) {
            if (houses[key].length) {
                for (let i = 0; i < houses[key].length; i++) {
                    const longitude = houses[key][i];
                    result.Houses.push(
                        AstroCalculator.getHouseResult(longitude, i)
                    );
                    if (i == 0) {
                        result.Planets.Ascendant = AstroCalculator.getPlanetResult(
                            { longitude: longitude, longitudeSpeed: 0 },
                            'Ascendant'
                        );
                    } else if (i == 9) {
                        result.Planets.MC = AstroCalculator.getPlanetResult(
                            { longitude: longitude, longitudeSpeed: 0 },
                            'MC'
                        );
                    }
                }
            } else if (key === 'vertex') {
                const longitude = houses[key];
                result.Planets.Vertex = AstroCalculator.getPlanetResult(
                    { longitude: longitude, longitudeSpeed: 0 },
                    'Vertex'
                );
            }
        }

        result.Planets.PurpleAir = AstroCalculator.getResult(
            PurpleAirCalculator.calculate(day, month, year)
        );
        result.Planets.PurpleAir.name = 'PurpleAir';

        for (let p in result.Planets) {
            this.getHouseOfPlanet(result, p);
        }

        result.Planets.PartOfFortune = AstroCalculator.getPlanetResult(
            {
                longitude: this.getPartOfFortuneLongitude(result),
                longitudeSpeed: 0
            },
            'PartOfFortune'
        );

        this.getHouseOfPlanet(result, 'PartOfFortune');

        this.findAspects(result);
        return result;
    }

    static getJulidanDayUT(day, month, year, hour, minute, timezone) {
        const d = swisseph.swe_utc_time_zone(
            year,
            month,
            day,
            hour,
            minute,
            0,
            timezone
        );
        return swisseph.swe_utc_to_jd(
            d.year,
            d.month,
            d.day,
            d.hour,
            d.minute,
            d.second,
            swisseph.SE_GREG_CAL
        ).julianDayUT;
    }

    static getPlanetResult(body, name) {
        const longitude = body.longitude;
        let result = AstroCalculator.getResult(longitude);
        result.retrograde = body.longitudeSpeed < 0;
        result.name = name;
        return result;
    }

    static getHouseResult(longitude, index) {
        let result = AstroCalculator.getResult(longitude);
        result.degree = result.longitude;
        result.house = index + 1;
        return result;
    }

    static getResult(longitude) {
        const sign = AstroHelper.getSign(longitude);
        const signName = AstroHelper.getSignName(sign);
        const degree = AstroHelper.getDegreeInSign(longitude);
        const minute = AstroHelper.getMinute(longitude);
        const position_string = degree + '\xB0' + minute;
        return {
            longitude: longitude,
            sign: sign,
            signName: signName,
            degrees: degree,
            minutes: minute,
            position_string: position_string
        };
    }

    static getHouseOfPlanet(result, p) {
        const originalPlantLongitude = result.Planets[p].longitude;
        for (let i = 0; i < result.Houses.length; i++) {
            const currentHouse = result.Houses[i];
            const nextIndex = i == result.Houses.length - 1 ? 0 : i + 1;
            const nextHouse = result.Houses[nextIndex];
            const currentHouseLongitude = currentHouse.longitude;
            let nextHouseLongitude = nextHouse.longitude;
            let comparePlanetLongitude = originalPlantLongitude;
            if (currentHouseLongitude > nextHouseLongitude) {
                if (comparePlanetLongitude < nextHouseLongitude) {
                    comparePlanetLongitude = comparePlanetLongitude + 360;
                }
                nextHouseLongitude = nextHouseLongitude + 360;
            }
            if (
                currentHouseLongitude <= comparePlanetLongitude &&
                comparePlanetLongitude < nextHouseLongitude
            ) {
                result.Planets[p].house = nextIndex == 0 ? 1 : nextIndex;
            }
        }
    }

    static getPartOfFortuneLongitude(result) {
        const longAsc = result.Planets.Ascendant.longitude;
        const longSun = result.Planets.Sun.longitude;
        const longMoon = result.Planets.Moon.longitude;
        const houseSun = result.Planets.Sun.house;
        const sub = houseSun > 6 ? longMoon - longSun : longSun - longMoon;
        let longPartOfFortune = longAsc + sub;
        while (longPartOfFortune >= 360) {
            longPartOfFortune = longPartOfFortune - 360;
        }

        while (longPartOfFortune < 0) {
            longPartOfFortune = longPartOfFortune + 360;
        }

        return longPartOfFortune;
    }

    static findAspects(result) {
        let aspects = {};
        let planets = [];
        for (const key in result.Planets) {
            planets.push(result.Planets[key]);
        }
        for (let i = 0; i < planets.length - 1; i++) {
            const currentPlanet = planets[i];
            for (let j = i + 1; j < planets.length; j++) {
                const nextPlanet = planets[j];
                let dict = currentPlanet.longitude - nextPlanet.longitude;
                while (dict >= 180 + 10) {
                    dict = dict - 180;
                }
                while (dict <= -180 - 10) {
                    dict = dict + 180;
                }

                dict = Math.abs(dict);

                const aspect = this.getAspect(
                    dict,
                    currentPlanet.name,
                    nextPlanet.name
                );
                if (aspect) {
                    const key = currentPlanet.name + '_' + nextPlanet.name;
                    aspects[key] = {
                        name: key,
                        aspect: aspect,
                        l1: currentPlanet.longitude,
                        l2: nextPlanet.longitude,
                        p1: currentPlanet.name,
                        p2: nextPlanet.name
                    };
                }
            }
        }
        result.Aspects = aspects;
    }

    static getAspect(dict, n1, n2) {
        for (let i = 0; i < ASPECT_TYPE.length; i++) {
            const aspect = ASPECT_TYPE[i];
            let orb = aspect.orb;
            if (NO_ORB.includes(n1) || NO_ORB.includes(n2)) {
                orb = 0;
            }

            if (aspect.angle - orb <= dict && dict <= aspect.angle + orb) {
                return aspect;
            }
        }

        return false;
    }
}

module.exports = AstroCalculator;
