const DegreeHelper = require('./DegreeHelper');

const TIME_A_DAY = 1000 * 60 * 60 * 24;

const VO = 0.0352 / TIME_A_DAY;

const ANCHOR_DATE = new Date(2009, 6, 1);

const ANCHOR_XO = DegreeHelper.toFloatNumber(10 + 10 * 30, 35);

class PurpleAirCalculator {
    static calculate(day, month, year) {
        const dt = new Date(year, month, day);
        const different = dt.getTime() - ANCHOR_DATE.getTime();
        let longitude = ANCHOR_XO + different * VO;
        while (Math.abs(longitude) >= 360) {
            longitude = longitude >= 0 ? longitude - 360 : longitude + 360;
        }
        if (longitude < 0) {
            longitude = longitude + 360;
        }
        return longitude;
    }
}

module.exports = PurpleAirCalculator;
