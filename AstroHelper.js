const SIGN_NAMES = [
    'Bạch Dương',
    'Kim Ngưu',
    'Song Tử',
    'Cự Giải',
    'Sư Tử',
    'Xử Nữ',
    'Thiên Bình',
    'Bọ Cạp',
    'Nhân Mã',
    'Ma Kết',
    'Bảo Bình',
    'Song Ngư'
];

class AstroHelper {
    static getSign(longitude) {
        return Math.ceil(longitude / 30) || 0;
    }

    static getSignName(sign) {
        return SIGN_NAMES[sign - 1] || '';
    }

    static getDegreeInSign(longitude) {
        return Math.floor(longitude % 30);
    }

    static getMinute(longitude) {
        const norm = longitude % 30;
        const deg = Math.floor(norm);
        const odd = norm - deg;
        return Math.round(odd * 60);
    }
}

module.exports = AstroHelper;
